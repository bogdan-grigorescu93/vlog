﻿namespace vLog
{
    public class EventModel
    {
        public double SecondsPassed { get; set; }
        public string EventName { get; set; }
        public string EventID { get; set; }
        public int LineNumber { get; set; }
        public string FullLine { get; set; }
    }

    public class EventDiff
    {
        public EventModel First { get; set; }
        public EventModel Second { get; set; }
        public double Diff { get; set; }
    }
}
