﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vLog
{
    public class VLog
    {
        private List<EventModel> _events;

        public void ReadFile(string filePath)
        {
            // TODO(bogdan): ADD validators

            var lines = File.ReadLines(filePath).Skip(5).ToList();

            _events = lines
                .Select((line, index) => new { FullLine = line, Index = index, Words = line.Split(null) })
                .Where(l => l.Words[2] == "CANFD") // TODO(bogdan): Find a better way
                .Select(l =>
                {
                    var seconds = l.Words[1];
                    var eventID = l.Words[14];
                    var eventName = l.Words[16];

                    return new EventModel
                    {
                        SecondsPassed = Double.Parse(seconds),
                        EventID = eventID,
                        EventName = eventName,
                        LineNumber = l.Index + 5,
                        FullLine = l.FullLine
                    };
                }).ToList();
        }

        public List<EventDiff> GetEvents(string eventID, double delta)
        {
            if (_events == null)
            {
                return null;
            }

            var filteredEvents = new List<EventDiff>();

            var sameIDEvents = _events
                .Where(e => e.EventID == eventID)
                .ToList();
                
            for (int i = 0; i < sameIDEvents.Count - 1; ++i)
            {
                var current = sameIDEvents[i];
                var next = sameIDEvents[i + 1];

                var diff = next.SecondsPassed - current.SecondsPassed;
                if (diff >= delta)
                {
                    filteredEvents.Add(new EventDiff
                    {
                        First = current,
                        Second = next,
                        Diff = diff
                    });
                }
            }

            return filteredEvents;
        }
    }


}
