﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vLog;

namespace UI
{
    public partial class UI : MaterialForm
    {
        private VLog _vlog;

        public UI()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            this.MaximizeBox = false;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.BlueGrey800, 
                Primary.BlueGrey900, 
                Primary.BlueGrey500, 
                Accent.LightBlue200, 
                TextShade.WHITE);

            _vlog = new VLog();

            // init firstPanel
            LoadFirstPanel();
        }

        private void loadAscBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _vlog.ReadFile(openFileDialog.FileName);

                LoadMainPanel();
            }
        }

        private void deltaLbl_Click(object sender, EventArgs e)
        {

        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            LoadFirstPanel();
        }

        private void LoadFirstPanel()
        {
            loadAscBtn.Left = (int)(loadAscPanel.Width * .5f - loadAscBtn.Width * 0.5f);
            loadAscBtn.Top = (int)(loadAscPanel.Height * .5f - loadAscBtn.Height * 0.5f);
            loadAscPanel.BringToFront();
            backBtn.Hide();
            pcanDropDown.SelectedItem = null;
            deltaTb.Text = string.Empty;
            diffRTb.Text = string.Empty;
        }

        private void LoadMainPanel()
        {
            backBtn.Show();
            mainPanel.BringToFront();

            var events = _vlog.GetEventIDs();
            pcanDropDown.Items.AddRange(events.ToArray());
        }

        private void viewResultsBtn_Click(object sender, EventArgs e)
        {
            var pcanID = (pcanDropDown.SelectedItem as ComboItem)?.Value;
            var deltaString = deltaTb.Text;

            if (string.IsNullOrWhiteSpace(pcanID) || string.IsNullOrWhiteSpace(deltaString))
            {
                return;
            }

            double delta;
            if (double.TryParse(deltaString, out delta))
            {
                var uiResult = _vlog.GetEvents(pcanID, delta);

                var events = uiResult.EventDiffs;
                var diffs = events.Select(p => $"{p.First.SecondsPassed.ToString("F6")} --- {p.Second.SecondsPassed.ToString("F6")} \t {p.Diff.ToString("F6")}");
                diffRTb.Lines = diffs.ToArray();
            }

        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void pcanTb_Click(object sender, EventArgs e)
        {

        }

        private void pcanDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void allLinesRTb_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
