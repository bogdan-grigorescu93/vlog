﻿using System.Collections.Generic;

namespace vLog
{
    public class EventModel
    {
        public double SecondsPassed { get; set; }
        public string EventName { get; set; }
        public string EventID { get; set; }
        public int LineNumber { get; set; }
    }

    public class EventDiff
    {
        public EventModel First { get; set; }
        public EventModel Second { get; set; }
        public double Diff { get; set; }
    }

    public class Result
    {
        public List<EventDiff> EventDiffs { get; set; }
    }

    public class ComboItem
    {
        public string Text { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
