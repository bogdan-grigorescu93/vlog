﻿namespace UI
{
    partial class UI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadAscBtn = new MaterialSkin.Controls.MaterialRaisedButton();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.loadAscPanel = new System.Windows.Forms.Panel();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.pcanDropDown = new System.Windows.Forms.ComboBox();
            this.diffRTb = new System.Windows.Forms.RichTextBox();
            this.viewResultsBtn = new MaterialSkin.Controls.MaterialRaisedButton();
            this.deltaTb = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.deltaLbl = new MaterialSkin.Controls.MaterialLabel();
            this.pcanLbl = new MaterialSkin.Controls.MaterialLabel();
            this.backBtn = new MaterialSkin.Controls.MaterialRaisedButton();
            this.loadAscPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadAscBtn
            // 
            this.loadAscBtn.Depth = 0;
            this.loadAscBtn.Location = new System.Drawing.Point(471, 197);
            this.loadAscBtn.Margin = new System.Windows.Forms.Padding(4);
            this.loadAscBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.loadAscBtn.Name = "loadAscBtn";
            this.loadAscBtn.Primary = true;
            this.loadAscBtn.Size = new System.Drawing.Size(156, 39);
            this.loadAscBtn.TabIndex = 1;
            this.loadAscBtn.Text = "Load ASC";
            this.loadAscBtn.UseVisualStyleBackColor = true;
            this.loadAscBtn.Click += new System.EventHandler(this.loadAscBtn_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "ascFile";
            // 
            // loadAscPanel
            // 
            this.loadAscPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loadAscPanel.Controls.Add(this.loadAscBtn);
            this.loadAscPanel.Location = new System.Drawing.Point(0, 79);
            this.loadAscPanel.Margin = new System.Windows.Forms.Padding(4);
            this.loadAscPanel.Name = "loadAscPanel";
            this.loadAscPanel.Size = new System.Drawing.Size(1067, 462);
            this.loadAscPanel.TabIndex = 2;
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainPanel.Controls.Add(this.pcanDropDown);
            this.mainPanel.Controls.Add(this.diffRTb);
            this.mainPanel.Controls.Add(this.viewResultsBtn);
            this.mainPanel.Controls.Add(this.deltaTb);
            this.mainPanel.Controls.Add(this.deltaLbl);
            this.mainPanel.Controls.Add(this.pcanLbl);
            this.mainPanel.Location = new System.Drawing.Point(0, 78);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(4);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1067, 542);
            this.mainPanel.TabIndex = 2;
            // 
            // pcanDropDown
            // 
            this.pcanDropDown.FormattingEnabled = true;
            this.pcanDropDown.Location = new System.Drawing.Point(111, 94);
            this.pcanDropDown.Margin = new System.Windows.Forms.Padding(4);
            this.pcanDropDown.Name = "pcanDropDown";
            this.pcanDropDown.Size = new System.Drawing.Size(325, 24);
            this.pcanDropDown.TabIndex = 8;
            this.pcanDropDown.SelectedIndexChanged += new System.EventHandler(this.pcanDropDown_SelectedIndexChanged);
            // 
            // diffRTb
            // 
            this.diffRTb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.diffRTb.BackColor = System.Drawing.Color.White;
            this.diffRTb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.diffRTb.Location = new System.Drawing.Point(548, 5);
            this.diffRTb.Margin = new System.Windows.Forms.Padding(4);
            this.diffRTb.Name = "diffRTb";
            this.diffRTb.ReadOnly = true;
            this.diffRTb.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.diffRTb.Size = new System.Drawing.Size(515, 458);
            this.diffRTb.TabIndex = 7;
            this.diffRTb.Text = "";
            // 
            // viewResultsBtn
            // 
            this.viewResultsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.viewResultsBtn.Depth = 0;
            this.viewResultsBtn.Location = new System.Drawing.Point(19, 279);
            this.viewResultsBtn.Margin = new System.Windows.Forms.Padding(4);
            this.viewResultsBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.viewResultsBtn.Name = "viewResultsBtn";
            this.viewResultsBtn.Primary = true;
            this.viewResultsBtn.Size = new System.Drawing.Size(212, 37);
            this.viewResultsBtn.TabIndex = 4;
            this.viewResultsBtn.Text = "View results";
            this.viewResultsBtn.UseVisualStyleBackColor = true;
            this.viewResultsBtn.Click += new System.EventHandler(this.viewResultsBtn_Click);
            // 
            // deltaTb
            // 
            this.deltaTb.Depth = 0;
            this.deltaTb.Hint = "Insert delta ";
            this.deltaTb.Location = new System.Drawing.Point(111, 178);
            this.deltaTb.Margin = new System.Windows.Forms.Padding(4);
            this.deltaTb.MouseState = MaterialSkin.MouseState.HOVER;
            this.deltaTb.Name = "deltaTb";
            this.deltaTb.PasswordChar = '\0';
            this.deltaTb.SelectedText = "";
            this.deltaTb.SelectionLength = 0;
            this.deltaTb.SelectionStart = 0;
            this.deltaTb.Size = new System.Drawing.Size(327, 28);
            this.deltaTb.TabIndex = 3;
            this.deltaTb.UseSystemPasswordChar = false;
            // 
            // deltaLbl
            // 
            this.deltaLbl.AutoSize = true;
            this.deltaLbl.Depth = 0;
            this.deltaLbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.deltaLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.deltaLbl.Location = new System.Drawing.Point(15, 183);
            this.deltaLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.deltaLbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.deltaLbl.Name = "deltaLbl";
            this.deltaLbl.Size = new System.Drawing.Size(53, 24);
            this.deltaLbl.TabIndex = 2;
            this.deltaLbl.Text = "Delta";
            this.deltaLbl.Click += new System.EventHandler(this.deltaLbl_Click);
            // 
            // pcanLbl
            // 
            this.pcanLbl.AutoSize = true;
            this.pcanLbl.Depth = 0;
            this.pcanLbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.pcanLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pcanLbl.Location = new System.Drawing.Point(15, 96);
            this.pcanLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pcanLbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.pcanLbl.Name = "pcanLbl";
            this.pcanLbl.Size = new System.Drawing.Size(60, 24);
            this.pcanLbl.TabIndex = 0;
            this.pcanLbl.Text = "PCAN";
            // 
            // backBtn
            // 
            this.backBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.backBtn.Depth = 0;
            this.backBtn.Location = new System.Drawing.Point(951, 42);
            this.backBtn.Margin = new System.Windows.Forms.Padding(4);
            this.backBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.backBtn.Name = "backBtn";
            this.backBtn.Primary = true;
            this.backBtn.Size = new System.Drawing.Size(100, 28);
            this.backBtn.TabIndex = 3;
            this.backBtn.Text = "Back";
            this.backBtn.UseVisualStyleBackColor = true;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.loadAscPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UI";
            this.Text = "vLog";
            this.loadAscPanel.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MaterialSkin.Controls.MaterialRaisedButton loadAscBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel loadAscPanel;
        private System.Windows.Forms.Panel mainPanel;
        private MaterialSkin.Controls.MaterialSingleLineTextField deltaTb;
        private MaterialSkin.Controls.MaterialLabel deltaLbl;
        private MaterialSkin.Controls.MaterialLabel pcanLbl;
        private MaterialSkin.Controls.MaterialRaisedButton backBtn;
        private MaterialSkin.Controls.MaterialRaisedButton viewResultsBtn;
        private System.Windows.Forms.RichTextBox diffRTb;
        private System.Windows.Forms.ComboBox pcanDropDown;
    }
}

